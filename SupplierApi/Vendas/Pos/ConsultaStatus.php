<?php
declare(strict_types=1);

namespace SupplierApi\Vendas\Pos;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class ConsultaStatus
{
    protected $client;
    
    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }
    
    public function send(string $codGrupo, string $clientId, string $cnpjCpf, string $codTransacao): array
    {
        try{
            $response = $this->client->get('posvendas/' . $codGrupo . '/' . $cnpjCpf . '/' . $codTransacao, [
                'Content-Type' => 'application/json',
                'client_id' => $clientId
            ]);
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }
        
        return json_decode($response->getBody());
    }
}
