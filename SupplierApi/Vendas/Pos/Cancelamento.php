<?php
declare(strict_types=1);

namespace SupplierApi\Vendas\Pos;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use SupplierApi\Serializers\Cancelamento as CancelamentoSerializer;

class Cancelamento
{
    protected $client;
    
    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }
    
    public function send(string $codGrupo, string $clientId, string $chaveIdemp, CancelamentoSerializer $cancelamento): array
    {
        try{
            $response = $this->client->post('devolucoes/' . $codGrupo . '/', [
                'Content-Type' => 'application/json',
                'client_id' => $clientId,
                'Chave-Idemp' => $chaveIdemp
            ], $cancelamento->json());
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }
        
        return json_decode($response->getBody());
    }
}
