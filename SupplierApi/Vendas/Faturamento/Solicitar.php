<?php
declare(strict_types=1);

namespace SupplierApi\Vendas\Faturamento;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use SupplierApi\Serializers\Faturamento as FaturamentoSerializer;

class Solicitar
{
    protected $client;
    
    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }
    
    public function send(string $codGrupo, string $clientId, string $chaveIdemp, FaturamentoSerializer $faturamento): array
    {
        try{
            $response = $this->client->post('faturamentos/' . $codGrupo . '/', [
                'Content-Type' => 'application/json',
                'client_id' => $clientId,
                'Chave-Idemp' => $chaveIdemp
            ], $faturamento->json());
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }
        
        return json_decode($response->getBody());
    }
}
