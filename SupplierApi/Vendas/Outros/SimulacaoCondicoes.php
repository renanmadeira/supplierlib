<?php
declare(strict_types=1);

namespace SupplierApi\Vendas\Outros;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use SupplierApi\Serializers\SimularCondicoes as SimularCondicoesSerializer;

class SimulacaoCondicoes
{
    protected $client;
    
    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }
    
    public function send(string $codGrupo, string $clientId, SimularCondicoesSerializer $simulacaoCondicoes): array
    {
        try{
            $response = $this->client->post('simulacao/' . $codGrupo . '/', [
                'Content-Type' => 'application/json',
                'client_id' => $clientId
            ], $simulacaoCondicoes->json());
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }
        
        return json_decode($response->getBody());
    }
}
