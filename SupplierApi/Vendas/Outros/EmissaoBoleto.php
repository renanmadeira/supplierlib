<?php
declare(strict_types=1);

namespace SupplierApi\Vendas\Outros;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class EmissaoBoleto
{
    protected $client;
    
    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }
    
    public function send(
        string $codGrupo,
        string $clientId,
        string $cnpjCpf,
        string $codTransacao,
        int $numeroParcela,
        string $tipoChave
    ): array
    {
        try{
            $response = $this->client->get('boletos/' . $codGrupo . '/' . $cnpjCpf . '/' . $codTransacao, [
                'Content-Type' => 'application/json',
                'client_id' => $clientId,
                'query' => [
                    'numeroParcela' => $numeroParcela,
                    'tipoChave' => $tipoChave
                ]
            ]);
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }
        
        return json_decode($response->getBody());
    }
}
