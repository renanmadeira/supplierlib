<?
declare(strict_types=1);

namespace SupplierApi\Serializers;


class AlteracaoTipoCliente
{
    protected $tipoCliente;

    function __construct($data)
    {
        try {
            setTipoCliente($data['tipoCliente']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize()
    {
        return [
            'tipoCliente' => getTipoCliente(),
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getTipoCliente(): string
    {
        return $this->tipoCliente;
    }

    public function setTipoCliente(string $tipoCliente): void
    {
        $this->tipoCliente = $tipoCliente;
    }
}
