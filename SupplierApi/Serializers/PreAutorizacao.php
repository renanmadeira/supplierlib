<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

use SupplierApi\Serializers\Taxas;

class PreAutorizacao
{    
    protected $cnpjCpf;
    protected $cnpjParceiro;
    protected $codigoPreAutorizacao;
    protected $numeroPedido;
    protected $valorPreAutorizacao;
    protected $numeroDiasPrimeiroVencimento;
    protected $numeroDiasEntreParcelas;
    protected $numeroParcelas;
    protected $tipoParcelamento;
    protected $tipoTransacao;
    protected $tipoRecebimentoParceiro;
    protected $prazoRecebimentoParceiro;
    protected $taxas = [];

    function __construct(array $data)
    {
        try {
            setCnpjCpf($data['cnpjCpf']);
            setCnpjParceiro($data['cnpjParceiro']);
            setCodigoPreAutorizacao($data['codigoPreAutorizacao']);
            setNumeroPedido($data['numeroPedido']);
            setValorPreAutorizacao($data['valorPreAutorizacao']);
            setNumeroDiasPrimeiroVencimento($data['numeroDiasPrimeiroVencimento']);
            setNumeroDiasEntreParcelas($data['numeroDiasEntreParcelas']);
            setNumeroParcelas($data['numeroParcelas']);
            setTipoParcelamento($data['tipoParcelamento']);
            setTipoTransacao($data['tipoTransacao']);
            setTipoRecebimentoParceiro($data['tipoRecebimentoParceiro']);
            setPrazoRecebimentoParceiro($data['prazoRecebimentoParceiro']);
            setTaxas($data['taxas']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'cnpjCpf' => getCnpjCpf(),
            'cnpjParceiro' => getCnpjParceiro(),
            'codigoPreAutorizacao' => getCodigoPreAutorizacao(),
            'numeroPedido' => getNumeroPedido(),
            'valorPreAutorizacao' => getValorPreAutorizacao(),
            'numeroDiasPrimeiroVencimento' => getNumeroDiasPrimeiroVencimento(),
            'numeroDiasEntreParcelas' => getNumeroDiasEntreParcelas(),
            'numeroParcelas' => getNumeroParcelas(),
            'tipoParcelamento' => getTipoParcelamento(),
            'tipoTransacao' => getTipoTransacao(),
            'tipoRecebimentoParceiro' => getTipoRecebimentoParceiro(),
            'prazoRecebimentoParceiro' => getPrazoRecebimentoParceiro(),
            'taxas' => getTaxas()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getCnpjCpf(): string
    {
        return $this->cnpjCpf;
    }

    public function setCnpjCpf(string $cnpjCpf): void
    {
        $this->cnpjCpf = $cnpjCpf;
    }

    public function getCnpjParceiro(): string
    {
        return $this->cnpjParceiro;
    }

    public function setCnpjParceiro(string $cnpjParceiro): void
    {
        $this->cnpjParceiro = $cnpjParceiro;
    }

    public function getCodigoPreAutorizacao(): int
    {
        return $this->codigoPreAutorizacao;
    }

    public function setCodigoPreAutorizacao(int $codigoPreAutorizacao): void
    {
        $this->codigoPreAutorizacao = $codigoPreAutorizacao;
    }

    public function getNumeroPedido(): string
    {
        return $this->numeroPedido;
    }

    public function setNumeroPedido(string $numeroPedido): void
    {
        $this->numeroPedido = $numeroPedido;
    }

    public function getValorPreAutorizacao(): int
    {
        return $this->valorPreAutorizacao;
    }

    public function setValorPreAutorizacao(int $valorPreAutorizacao): void
    {
        $this->valorPreAutorizacao = $valorPreAutorizacao;
    }

    public function getNumeroDiasPrimeiroVencimento(): int
    {
        return $this->numeroDiasPrimeiroVencimento;
    }

    public function setNumeroDiasPrimeiroVencimento(int $numeroDiasPrimeiroVencimento): void
    {
        $this->numeroDiasPrimeiroVencimento = $numeroDiasPrimeiroVencimento;
    }

    public function getNumeroDiasEntreParcelas(): int
    {
        return $this->numeroDiasEntreParcelas;
    }

    public function setNumeroDiasEntreParcelas(int $numeroDiasEntreParcelas): void
    {
        $this->numeroDiasEntreParcelas = $numeroDiasEntreParcelas;
    }

    public function getNumeroParcelas(): int
    {
        return $this->numeroParcelas;
    }

    public function setNumeroParcelas(int $numeroParcelas): void
    {
        $this->numeroParcelas = $numeroParcelas;
    }

    public function getTipoParcelamento(): string
    {
        return $this->tipoParcelamento;
    }

    public function setTipoParcelamento(string $tipoParcelamento): void
    {
        $this->tipoParcelamento = $tipoParcelamento;
    }

    public function getTipoTransacao(): string
    {
        return $this->tipoTransacao;
    }

    public function setTipoTransacao(string $tipoTransacao): void
    {
        $this->tipoTransacao = $tipoTransacao;
    }

    public function getTipoRecebimentoParceiro(): string
    {
        return $this->tipoRecebimentoParceiro;
    }

    public function setTipoRecebimentoParceiro(string $tipoRecebimentoParceiro): void
    {
        $this->tipoRecebimentoParceiro = $tipoRecebimentoParceiro;
    }

    public function getPrazoRecebimentoParceiro(): int
    {
        return $this->prazoRecebimentoParceiro;
    }

    public function setPrazoRecebimentoParceiro(int $prazoRecebimentoParceiro): void
    {
        $this->prazoRecebimentoParceiro = $prazoRecebimentoParceiro;
    }

    public function getTaxas(): array
    {
        return $this->taxas;
    }

    public function setTaxas(array $taxas): void
    {
        foreach($taxas as $taxa) {
            $serializedTaxas = new Taxas($taxa);
            $this->taxas += $serializedTaxas->serialize();
        }
    }
}
