<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

class Taxas
{
    protected $tipoTaxa;
    protected $valorTaxa;

    function __construct(array $data)
    {
        try {
            setTipoTaxa($data['tipoTaxa']);
            setValorTaxa($data['valorTaxa']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'tipoTaxa' => getTipoTaxa(),
            'valorTaxa' => getValorTaxa()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getTipoTaxa(): string
    {
        return $this->tipoTaxa;
    }

    public function setTipoTaxa(string $tipoTaxa): void
    {
        $this->tipoTaxa = $tipoTaxa;
    }

    public function getValorTaxa(): int
    {
        return $this->valorTaxa;
    }

    public function setValorTaxa(int $valorTaxa): void
    {
        $this->valorTaxa = $valorTaxa;
    }
}
