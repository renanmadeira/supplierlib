<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

class Auth
{
    
    protected $grantType = 'password';
    protected $username;
    protected $password;

    function __construct(array $data)
    {
        try {
            if(!empty($data['grant_type'])) {
                setGrantType($data['grant_type']);
            }
            setUsername($data['username']);
            setPassword($data['password']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'grant_type' => getGrantType(),
            'username' => getUsername(),
            'password' => getPassword()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getGrantType(): string
    {
        return $this->grantType;
    }

    public function setGrantType(string $grantType): void
    {
        $this->grantType = $grantType;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }
}
