<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

class DadosSocios
{
    protected $cnpjCpf;
    protected $nomeRazaoSocial;
    protected $dataNascimentoFundacao;

    function __construct(array $data)
    {
        try {
            setCnpjCpf($data['cnpjCpf']);
            setNomeRazaoSocial($data['nomeRazaoSocial']);
            setDataNascimentoFundacao($data['dataNascimentoFundacao']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'cnpjCpf' => getCnpjCpf(),
            'nomeRazaoSocial' => getNomeRazaoSocial(),
            'dataNascimentoFundacao' => getDataNascimentoFundacao()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getCnpjCpf(): string
    {
        return $this->cnpjCpf;
    }

    public function setCnpjCpf(string $cnpjCpf): void
    {
        $this->cnpjCpf = $cnpjCpf;
    }

    public function getNomeRazaoSocial(): string
    {
        return $this->nomeRazaoSocial;
    }

    public function setNomeRazaoSocial(string $nomeRazaoSocial): void
    {
        $this->nomeRazaoSocial = $nomeRazaoSocial;
    }

    public function getDataNascimentoFundacao(): string
    {
        return $this->dataNascimentoFundacao;
    }

    public function setDataNascimentoFundacao(string $dataNascimentoFundacao): void
    {
        $this->dataNascimentoFundacao = $dataNascimentoFundacao;
    }
}
