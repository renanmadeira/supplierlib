<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

use SupplierApi\Serializers\HistoricoCompras;


class AlteracaoLimite
{
    protected $cnpjCpf;
    protected $limiteCompraParceiro;
    protected $limiteCompraSugerido;
    protected $solicitacaoEmergencial;
    protected $informacoesComplementares;
    protected $dadosCadastrais = [];
    protected $dadosSocios = [];
    protected $historicoCompras = [];

    function __construct($data)
    {
        try {
            setCnpjCpf($data['cnpjCpf']);
            setLimiteCompraParceiro($data['limiteCompraParceiro']);
            setLimiteCompraSugerido($data['limiteCompraSugerido']);
            setSolicitacaoEmergencial($data['solicitacaoEmergencial']);
            setInformacoesComplementares($data['informacoesComplementares']);
            setHistoricoCompras($data['historicoCompras']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize()
    {
        return [
            'cnpjCpf' => getCnpjCpf(),
            'limiteCompraParceiro' => getLimiteCompraParceiro(),
            'limiteCompraSugerido' => getLimiteCompraSugerido(),
            'solicitacaoEmergencial' => getSolicitacaoEmergencial(),
            'informacoesComplementares' => getInformacoesComplementares(),
            'historicoCompras' => getHistoricoCompras()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getCnpjCpf(): string
    {
        return $this->cnpjCpf;
    }

    public function setCnpjCpf(string $cnpjCpf): void
    {
        $this->cnpjCpf = $cnpjCpf;
    }

    public function getLimiteCompraParceiro(): int
    {
        return $this->limiteCompraParceiro;
    }

    public function setLimiteCompraParceiro(int $limiteCompraParceiro): void
    {
        $this->limiteCompraParceiro = $limiteCompraParceiro;
    }

    public function getLimiteCompraSugerido(): int
    {
        return $this->limiteCompraSugerido;
    }

    public function setLimiteCompraSugerido(int $limiteCompraSugerido): void
    {
        $this->limiteCompraSugerido = $limiteCompraSugerido;
    }

    public function getSolicitacaoEmergencial(): string
    {
        return $this->solicitacaoEmergencial;
    }

    public function setSolicitacaoEmergencial(string $solicitacaoEmergencial): void
    {
        $this->solicitacaoEmergencial = $solicitacaoEmergencial;
    }

    public function getInformacoesComplementares(): string
    {
        return $this->informacoesComplementares;
    }

    public function setInformacoesComplementares(string $informacoesComplementares): void
    {
        $this->informacoesComplementares = $informacoesComplementares;
    }

    public function getHistoricoCompras(): array
    {
        return $this->historicoCompras;
    }

    public function setHistoricoCompras(array $historicoCompras): void
    {
        foreach($historicoCompras as $historicoCompra) {
            $serializedHistoricoCompras = new HistoricoCompras($historicoCompra);
            $this->historicoCompras += $serializedHistoricoCompras->serialize();
        }
    }
}
