<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

use SupplierApi\Serializers\Taxas;

class Faturamento
{    
    protected $cnpjCpf;
    protected $cnpjParceiro;
    protected $codigoPreAutorizacao;
    protected $numeroPedido;
    protected $dataPrimeiroVencimento;
    protected $numeroDiasPrimeiroVencimento;
    protected $numeroDiasEntreParcelas;
    protected $numeroParcelas;
    protected $tipoParcelamento;
    protected $tipoTransacao;
    protected $tipoRecebimentoParceiro;
    protected $prazoRecebimentoParceiro;
    protected $numeroNotaFiscal;
    protected $valorNotaFiscal;
    protected $dataEmissaoNotaFiscal;
    protected $chaveAcessoNotaFiscal;
    protected $taxas = [];

    function __construct(array $data)
    {
        try {
            setCnpjCpf($data['cnpjCpf']);
            setCnpjParceiro($data['cnpjParceiro']);
            setCodigoPreAutorizacao($data['codigoPreAutorizacao']);
            setNumeroPedido($data['numeroPedido']);
            setDataPrimeiroVencimento($data['dataPrimeiroVencimento']);
            setNumeroDiasPrimeiroVencimento($data['numeroDiasPrimeiroVencimento']);
            setNumeroDiasEntreParcelas($data['numeroDiasEntreParcelas']);
            setNumeroParcelas($data['numeroParcelas']);
            setTipoParcelamento($data['tipoParcelamento']);
            setTipoTransacao($data['tipoTransacao']);
            setTipoRecebimentoParceiro($data['tipoRecebimentoParceiro']);
            setPrazoRecebimentoParceiro($data['prazoRecebimentoParceiro']);
            setNumeroNotaFiscal($data['numeroNotaFiscal']);
            setValorNotaFiscal($data['valorNotaFiscal']);
            setDataEmissaoNotaFiscal($data['dataEmissaoNotaFiscal']);
            setChaveAcessoNotaFiscal($data['chaveAcessoNotaFiscal']);
            setTaxas($data['taxas']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'cnpjCpf' => getCnpjCpf(),
            'cnpjParceiro' => getCnpjParceiro(),
            'codigoPreAutorizacao' => getCodigoPreAutorizacao(),
            'numeroPedido' => getNumeroPedido(),
            'dataPrimeiroVencimento' => getDataPrimeiroVencimento(),
            'numeroDiasPrimeiroVencimento' => getNumeroDiasPrimeiroVencimento(),
            'numeroDiasEntreParcelas' => getNumeroDiasEntreParcelas(),
            'numeroParcelas' => getNumeroParcelas(),
            'tipoParcelamento' => getTipoParcelamento(),
            'tipoTransacao' => getTipoTransacao(),
            'tipoRecebimentoParceiro' => getTipoRecebimentoParceiro(),
            'prazoRecebimentoParceiro' => getPrazoRecebimentoParceiro(),
            'numeroNotaFiscal' => getNumeroNotaFiscal(),
            'valorNotaFiscal' => getValorNotaFiscal(),
            'dataEmissaoNotaFiscal' => getDataEmissaoNotaFiscal(),
            'chaveAcessoNotaFiscal' => getChaveAcessoNotaFiscal(),
            'taxas' => getTaxas()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getCnpjCpf(): string
    {
        return $this->cnpjCpf;
    }

    public function setCnpjCpf(string $cnpjCpf): void
    {
        $this->cnpjCpf = $cnpjCpf;
    }

    public function getCnpjParceiro(): string
    {
        return $this->cnpjParceiro;
    }

    public function setCnpjParceiro(string $cnpjParceiro): void
    {
        $this->cnpjParceiro = $cnpjParceiro;
    }

    public function getCodigoPreAutorizacao(): int
    {
        return $this->codigoPreAutorizacao;
    }

    public function setCodigoPreAutorizacao(int $codigoPreAutorizacao): void
    {
        $this->codigoPreAutorizacao = $codigoPreAutorizacao;
    }

    public function getNumeroPedido(): string
    {
        return $this->numeroPedido;
    }

    public function setNumeroPedido(string $numeroPedido): void
    {
        $this->numeroPedido = $numeroPedido;
    }

    public function getDataPrimeiroVencimento(): string
    {
        return $this->dataPrimeiroVencimento;
    }

    public function setDataPrimeiroVencimento(string $dataPrimeiroVencimento): void
    {
        $this->dataPrimeiroVencimento = $dataPrimeiroVencimento;
    }

    public function getNumeroDiasPrimeiroVencimento(): int
    {
        return $this->numeroDiasPrimeiroVencimento;
    }

    public function setNumeroDiasPrimeiroVencimento(int $numeroDiasPrimeiroVencimento): void
    {
        $this->numeroDiasPrimeiroVencimento = $numeroDiasPrimeiroVencimento;
    }

    public function getNumeroDiasEntreParcelas(): int
    {
        return $this->numeroDiasEntreParcelas;
    }

    public function setNumeroDiasEntreParcelas(int $numeroDiasEntreParcelas): void
    {
        $this->numeroDiasEntreParcelas = $numeroDiasEntreParcelas;
    }

    public function getNumeroParcelas(): int
    {
        return $this->numeroParcelas;
    }

    public function setNumeroParcelas(int $numeroParcelas): void
    {
        $this->numeroParcelas = $numeroParcelas;
    }

    public function getTipoParcelamento(): string
    {
        return $this->tipoParcelamento;
    }

    public function setTipoParcelamento(string $tipoParcelamento): void
    {
        $this->tipoParcelamento = $tipoParcelamento;
    }

    public function getTipoTransacao(): string
    {
        return $this->tipoTransacao;
    }

    public function setTipoTransacao(string $tipoTransacao): void
    {
        $this->tipoTransacao = $tipoTransacao;
    }

    public function getTipoRecebimentoParceiro(): string
    {
        return $this->tipoRecebimentoParceiro;
    }

    public function setTipoRecebimentoParceiro(string $tipoRecebimentoParceiro): void
    {
        $this->tipoRecebimentoParceiro = $tipoRecebimentoParceiro;
    }

    public function getPrazoRecebimentoParceiro(): int
    {
        return $this->prazoRecebimentoParceiro;
    }

    public function setPrazoRecebimentoParceiro(int $prazoRecebimentoParceiro): void
    {
        $this->prazoRecebimentoParceiro = $prazoRecebimentoParceiro;
    }

    public function getNumeroNotaFiscal(): string
    {
        return $this->numeroNotaFiscal;
    }

    public function setNumeroNotaFiscal(string $numeroNotaFiscal): void
    {
        $this->numeroNotaFiscal = $numeroNotaFiscal;
    }

    public function getValorNotaFiscal(): int
    {
        return $this->valorNotaFiscal;
    }

    public function setValorNotaFiscal(int $valorNotaFiscal): void
    {
        $this->valorNotaFiscal = $valorNotaFiscal;
    }

    public function getDataEmissaoNotaFiscal(): string
    {
        return $this->dataEmissaoNotaFiscal;
    }

    public function setDataEmissaoNotaFiscal(string $dataEmissaoNotaFiscal): void
    {
        $this->dataEmissaoNotaFiscal = $dataEmissaoNotaFiscal;
    }

    public function getChaveAcessoNotaFiscal(): string
    {
        return $this->chaveAcessoNotaFiscal;
    }

    public function setChaveAcessoNotaFiscal(string $chaveAcessoNotaFiscal): void
    {
        $this->chaveAcessoNotaFiscal = $chaveAcessoNotaFiscal;
    }

    public function getTaxas(): array
    {
        return $this->taxas;
    }

    public function setTaxas(array $taxas): void
    {
        foreach($taxas as $taxa) {
            $serializedTaxas = new Taxas($taxa);
            $this->taxas += $serializedTaxas->serialize();
        }
    }
}
