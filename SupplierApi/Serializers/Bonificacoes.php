<?
declare(strict_types=1);

namespace SupplierApi\Serializers;


class Bonificacoes
{    
    protected $cnpjCpf;
    protected $codigoTransacao;
    protected $valorBonificacao;

    function __construct(array $data)
    {
        try {
            setCnpjCpf($data['cnpjCpf']);
            setCodigoTransacao($data['codigoTransacao']);
            setValorBonificacao($data['valorBonificacao']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'cnpjCpf' => getCnpjCpf(),
            'codigoTransacao' => getCodigoTransacao(),
            'valorBonificacao' => getValorBonificacao()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getCnpjCpf(): string
    {
        return $this->cnpjCpf;
    }

    public function setCnpjCpf(string $cnpjCpf): void
    {
        $this->cnpjCpf = $cnpjCpf;
    }

    public function getCodigoTransacao(): string
    {
        return $this->codigoTransacao;
    }

    public function setCodigoTransacao(string $codigoTransacao): void
    {
        $this->codigoTransacao = $codigoTransacao;
    }

    public function getValorBonificacao(): int
    {
        return $this->valorBonificacao;
    }

    public function setValorBonificacao(int $valorBonificacao): void
    {
        $this->valorBonificacao = $valorBonificacao;
    }
}
