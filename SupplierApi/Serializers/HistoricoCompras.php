<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

class HistoricoCompras
{
    protected $nf;  # Número da NF faturada para o cliente
    protected $dtem;  # Data de emissão da NF
    protected $dtvc;  # Data de vencimento da parcela
    protected $dtpg;  # Data de pagamento da parcela
    protected $vlnf;  # Valor total da NF
    protected $npc;  # Número de parcelas da NF
    protected $vlpc;  # Valor original da parcela
    protected $vlpg;  # Valor pago pelo cliente
    protected $pcq;  # Indicador de quitação da parcela (‘S’ o ‘N’)

    function __construct(array $data)
    {
        try {
            setNf($data['nf']);
            setDtem($data['dtem']);
            setDtvc($data['dtvc']);
            setDtpg($data['dtpg']);
            setVlnf($data['vlnf']);
            setNpc($data['npc']);
            setVlpc($data['vlpc']);
            setVlpg($data['vlpg']);
            setPcq($data['pcq']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'nf' => getNf(),
            'dtem' => getDtem(),
            'dtvc' => getDtvc(),
            'dtpg' => getDtpg(),
            'vlnf' => getVlnf(),
            'npc' => getNpc(),
            'vlpc' => getVlpc(),
            'vlpg' => getVlpg(),
            'pcq' => getPcq()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getNf(): string
    {
        return $this->nf;
    }

    public function setNf(string $nf): void
    {
        $this->nf = $nf;
    }

    public function getDtem(): string
    {
        return $this->dtem;
    }

    public function setDtem(string $dtem): void
    {
        $this->dtem = $dtem;
    }

    public function getDtvc(): string
    {
        return $this->dtvc;
    }

    public function setDtvc(string $dtvc): void
    {
        $this->dtvc = $dtvc;
    }

    public function getDtpg(): string
    {
        return $this->dtpg;
    }

    public function setDtpg(string $dtpg): void
    {
        $this->dtpg = $dtpg;
    }

    public function getVlnf(): int
    {
        return $this->vlnf;
    }

    public function setVlnf(int $vlnf): void
    {
        $this->vlnf = $vlnf;
    }

    public function getNpc(): int
    {
        return $this->npc;
    }

    public function setNpc(int $npc): void
    {
        $this->npc = $npc;
    }

    public function getVlpc(): int
    {
        return $this->vlpc;
    }

    public function setVlpc(int $vlpc): void
    {
        $this->vlpc = $vlpc;
    }

    public function getVlpg(): int
    {
        return $this->vlpg;
    }

    public function setVlpg(int $vlpg): void
    {
        $this->vlpg = $vlpg;
    }

    public function getPcq(): string
    {
        return $this->pcq;
    }

    public function setPcq(string $pcq): void
    {
        $this->pcq = $pcq;
    }
}
