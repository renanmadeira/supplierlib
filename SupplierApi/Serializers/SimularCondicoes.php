<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

use SupplierApi\Serializers\Taxas;

class SimularCondicoes
{    
    protected $cnpjCpf;
    protected $cnpjParceiro;
    protected $numeroDiasPrimeiroVencimento;
    protected $numeroDiasEntreParcelas;
    protected $numeroParcelas;
    protected $tipoParcelamento;
    protected $valorSimulacao;
    protected $tipoTransacao;
    protected $tipoRecebimentoParceiro;
    protected $prazoRecebimentoParceiro;
    protected $taxas = [];

    function __construct(array $data)
    {
        try {
            setCnpjCpf($data['cnpjCpf']);
            setCnpjParceiro($data['cnpjParceiro']);
            setNumeroDiasPrimeiroVencimento($data['numeroDiasPrimeiroVencimento']);
            setNumeroDiasEntreParcelas($data['numeroDiasEntreParcelas']);
            setNumeroParcelas($data['numeroParcelas']);
            setTipoParcelamento($data['tipoParcelamento']);
            setValorSimulacao($data['valorSimulacao']);
            setTipoTransacao($data['tipoTransacao']);
            setTipoRecebimentoParceiro($data['tipoRecebimentoParceiro']);
            setPrazoRecebimentoParceiro($data['prazoRecebimentoParceiro']);
            setTaxas($data['taxas']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'cnpjCpf' => getCnpjCpf(),
            'cnpjParceiro' => getCnpjParceiro(),
            'numeroDiasPrimeiroVencimento' => getNumeroDiasPrimeiroVencimento(),
            'numeroDiasEntreParcelas' => getNumeroDiasEntreParcelas(),
            'numeroParcelas' => getNumeroParcelas(),
            'tipoParcelamento' => getTipoParcelamento(),
            'valorSimulacao' => getValorSimulacao(),
            'tipoTransacao' => getTipoTransacao(),
            'tipoRecebimentoParceiro' => getTipoRecebimentoParceiro(),
            'prazoRecebimentoParceiro' => getPrazoRecebimentoParceiro(),
            'taxas' => getTaxas()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getCnpjCpf(): string
    {
        return $this->cnpjCpf;
    }

    public function setCnpjCpf(string $cnpjCpf): void
    {
        $this->cnpjCpf = $cnpjCpf;
    }

    public function getCnpjParceiro(): string
    {
        return $this->cnpjParceiro;
    }

    public function setCnpjParceiro(string $cnpjParceiro): void
    {
        $this->cnpjParceiro = $cnpjParceiro;
    }

    public function getNumeroDiasPrimeiroVencimento(): int
    {
        return $this->numeroDiasPrimeiroVencimento;
    }

    public function setNumeroDiasPrimeiroVencimento(int $numeroDiasPrimeiroVencimento): void
    {
        $this->numeroDiasPrimeiroVencimento = $numeroDiasPrimeiroVencimento;
    }

    public function getNumeroDiasEntreParcelas(): int
    {
        return $this->numeroDiasEntreParcelas;
    }

    public function setNumeroDiasEntreParcelas(int $numeroDiasEntreParcelas): void
    {
        $this->numeroDiasEntreParcelas = $numeroDiasEntreParcelas;
    }

    public function getNumeroParcelas(): int
    {
        return $this->numeroParcelas;
    }

    public function setNumeroParcelas(int $numeroParcelas): void
    {
        $this->numeroParcelas = $numeroParcelas;
    }

    public function getTipoParcelamento(): string
    {
        return $this->tipoParcelamento;
    }

    public function setTipoParcelamento(string $tipoParcelamento): void
    {
        $this->tipoParcelamento = $tipoParcelamento;
    }

    public function getValorSimulacao(): int
    {
        return $this->valorSimulacao;
    }

    public function setValorSimulacao(int $valorSimulacao): void
    {
        $this->valorSimulacao = $valorSimulacao;
    }

    public function getTipoTransacao(): string
    {
        return $this->tipoTransacao;
    }

    public function setTipoTransacao(string $tipoTransacao): void
    {
        $this->tipoTransacao = $tipoTransacao;
    }

    public function getTipoRecebimentoParceiro(): string
    {
        return $this->tipoRecebimentoParceiro;
    }

    public function setTipoRecebimentoParceiro(string $tipoRecebimentoParceiro): void
    {
        $this->tipoRecebimentoParceiro = $tipoRecebimentoParceiro;
    }

    public function getPrazoRecebimentoParceiro(): int
    {
        return $this->prazoRecebimentoParceiro;
    }

    public function setPrazoRecebimentoParceiro(int $prazoRecebimentoParceiro): void
    {
        $this->prazoRecebimentoParceiro = $prazoRecebimentoParceiro;
    }

    public function getTaxas(): array
    {
        return $this->taxas;
    }

    public function setTaxas(array $taxas): void
    {
        foreach($taxas as $taxa) {
            $serializedTaxas = new Taxas($taxa);
            $this->taxas += $serializedTaxas->serialize();
        }
    }
}
