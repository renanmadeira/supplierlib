<?
declare(strict_types=1);

namespace SupplierApi\Serializers;

class DadosCadastrais
{
    protected $razaoSocial;
    protected $logradouro;
    protected $numero;
    protected $complemento;
    protected $bairro;
    protected $cidade;
    protected $uf;
    protected $cep;
    protected $nomeContatoFinanceiro;
    protected $dddContatoFinanceiro;
    protected $telContatoFinanceiro;
    protected $ramalContatoFinanceiro;
    protected $dddCelularFinanceiro;
    protected $telCelularFinanceiro;
    protected $emailContatoCompras;
    protected $emailContatoFinanceiro;
    protected $clienteDesde;
    protected $tipoCliente;

    function __construct(array $data)
    {
        try {
            setRazaoSocial($data['razaoSocial']);
            setLogradouro($data['logradouro']);
            setNumero($data['numero']);
            setComplemento($data['complemento']);
            setBairro($data['bairro']);
            setCidade($data['cidade']);
            setUf($data['uf']);
            setCep($data['cep']);
            setNomeContatoFinanceiro($data['nomeContatoFinanceiro']);
            setDddContatoFinanceiro($data['dddContatoFinanceiro']);
            setTelContatoFinanceiro($data['telContatoFinanceiro']);
            setRamalContatoFinanceiro($data['ramalContatoFinanceiro']);
            setDddCelularFinanceiro($data['dddCelularFinanceiro']);
            setTelCelularFinanceiro($data['telCelularFinanceiro']);
            setEmailContatoCompras($data['emailContatoCompras']);
            setEmailContatoFinanceiro($data['emailContatoFinanceiro']);
            setClienteDesde($data['clienteDesde']);
            setTipoCliente($data['tipoCliente']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'razaoSocial' => getRazaoSocial(),
            'logradouro' => getLogradouro(),
            'numero' => getNumero(),
            'complemento' => getComplemento(),
            'bairro' => getBairro(),
            'cidade' => getCidade(),
            'uf' => getUf(),
            'cep' => getCep(),
            'nomeContatoFinanceiro' => getNomeContatoFinanceiro(),
            'dddContatoFinanceiro' => getDddContatoFinanceiro(),
            'telContatoFinanceiro' => getTelContatoFinanceiro(),
            'ramalContatoFinanceiro' => getRamalContatoFinanceiro(),
            'dddCelularFinanceiro' => getDddCelularFinanceiro(),
            'telCelularFinanceiro' => getTelCelularFinanceiro(),
            'emailContatoCompras' => getEmailContatoCompras(),
            'emailContatoFinanceiro' => getEmailContatoFinanceiro(),
            'clienteDesde' => getClienteDesde(),
            'tipoCliente' => getTipoCliente()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getRazaoSocial(): string
    {
        return $this->razaoSocial;
    }

    public function setRazaoSocial(string $razaoSocial): void
    {
        $this->razaoSocial = $razaoSocial;
    }

    public function getLogradouro(): string
    {
        return $this->logradouro;
    }

    public function setLogradouro(string $logradouro): void
    {
        $this->logradouro = $logradouro;
    }

    public function getNumero(): string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): void
    {
        $this->numero = $numero;
    }

    public function getComplemento(): string
    {
        return $this->complemento;
    }

    public function setComplemento(string $complemento): void
    {
        $this->complemento = $complemento;
    }

    public function getBairro(): string
    {
        return $this->bairro;
    }

    public function setBairro(string $bairro): void
    {
        $this->bairro = $bairro;
    }

    public function getCidade(): string
    {
        return $this->cidade;
    }

    public function setCidade(string $cidade): void
    {
        $this->cidade = $cidade;
    }

    public function getUf(): string
    {
        return $this->uf;
    }

    public function setUf(string $uf): void
    {
        $this->uf = $uf;
    }

    public function getCep(): string
    {
        return $this->cep;
    }

    public function setCep(string $cep): void
    {
        $this->cep = $cep;
    }

    public function getNomeContatoFinanceiro(): string
    {
        return $this->nomeContatoFinanceiro;
    }

    public function setNomeContatoFinanceiro(string $nomeContatoFinanceiro): void
    {
        $this->nomeContatoFinanceiro = $nomeContatoFinanceiro;
    }

    public function getDddContatoFinanceiro(): string
    {
        return $this->dddContatoFinanceiro;
    }

    public function setDddContatoFinanceiro(string $dddContatoFinanceiro): void
    {
        $this->dddContatoFinanceiro = $dddContatoFinanceiro;
    }

    public function getTelContatoFinanceiro(): string
    {
        return $this->telContatoFinanceiro;
    }

    public function setTelContatoFinanceiro(string $telContatoFinanceiro): void
    {
        $this->telContatoFinanceiro = $telContatoFinanceiro;
    }

    public function getRamalContatoFinanceiro(): string
    {
        return $this->ramalContatoFinanceiro;
    }

    public function setRamalContatoFinanceiro(string $ramalContatoFinanceiro): void
    {
        $this->ramalContatoFinanceiro = $ramalContatoFinanceiro;
    }

    public function getDddCelularFinanceiro(): string
    {
        return $this->dddCelularFinanceiro;
    }

    public function setDddCelularFinanceiro(string $dddCelularFinanceiro): void
    {
        $this->dddCelularFinanceiro = $dddCelularFinanceiro;
    }

    public function getTelCelularFinanceiro(): string
    {
        return $this->telCelularFinanceiro;
    }

    public function setTelCelularFinanceiro(string $telCelularFinanceiro): void
    {
        $this->telCelularFinanceiro = $telCelularFinanceiro;
    }

    public function getEmailContatoCompras(): string
    {
        return $this->emailContatoCompras;
    }

    public function setEmailContatoCompras(string $emailContatoCompras): void
    {
        $this->emailContatoCompras = $emailContatoCompras;
    }

    public function getEmailContatoFinanceiro(): string
    {
        return $this->emailContatoFinanceiro;
    }

    public function setEmailContatoFinanceiro(string $emailContatoFinanceiro): void
    {
        $this->emailContatoFinanceiro = $emailContatoFinanceiro;
    }

    public function getClienteDesde(): string
    {
        return $this->clienteDesde;
    }

    public function setClienteDesde(string $clienteDesde): void
    {
        $this->clienteDesde = $clienteDesde;
    }

    public function getTipoCliente(): string
    {
        return $this->tipoCliente;
    }

    public function setTipoCliente(string $tipoCliente): void
    {
        $this->tipoCliente = $tipoCliente;
    }
}
