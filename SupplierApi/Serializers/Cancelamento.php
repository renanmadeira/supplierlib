<?
declare(strict_types=1);

namespace SupplierApi\Serializers;


class Cancelamento
{    
    protected $cnpjCpf;
    protected $codigoTransacao;
    protected $valorDevolucao;

    function __construct(array $data)
    {
        try {
            setCnpjCpf($data['cnpjCpf']);
            setCodigoTransacao($data['codigoTransacao']);
            setValorDevolucao($data['valorDevolucao']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'cnpjCpf' => getCnpjCpf(),
            'codigoTransacao' => getCodigoTransacao(),
            'valorDevolucao' => getValorDevolucao()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getCnpjCpf(): string
    {
        return $this->cnpjCpf;
    }

    public function setCnpjCpf(string $cnpjCpf): void
    {
        $this->cnpjCpf = $cnpjCpf;
    }

    public function getCodigoTransacao(): string
    {
        return $this->codigoTransacao;
    }

    public function setCodigoTransacao(string $codigoTransacao): void
    {
        $this->codigoTransacao = $codigoTransacao;
    }

    public function getValorDevolucao(): int
    {
        return $this->valorDevolucao;
    }

    public function setValorDevolucao(int $valorDevolucao): void
    {
        $this->valorDevolucao = $valorDevolucao;
    }
}
