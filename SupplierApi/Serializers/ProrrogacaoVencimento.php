<?
declare(strict_types=1);

namespace SupplierApi\Serializers;


class ProrrogacaoVencimento
{    
    protected $cnpjCpf;
    protected $codigoTransacao;
    protected $numeroParcela;
    protected $numeroDiasProrrogacao;
    protected $novaDataVencimento;

    function __construct(array $data)
    {
        try {
            setCnpjCpf($data['cnpjCpf']);
            setCodigoTransacao($data['codigoTransacao']);
            setNumeroParcela($data['numeroParcela']);
            setNumeroDiasProrrogacao($data['numeroDiasProrrogacao']);
            setNovaDataVencimento($data['novaDataVencimento']);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function serialize(): array
    {
        return [
            'cnpjCpf' => getCnpjCpf(),
            'codigoTransacao' => getCodigoTransacao(),
            'numeroParcela' => getNumeroParcela(),
            'numeroDiasProrrogacao' => getNumeroDiasProrrogacao(),
            'novaDataVencimento' => getNovaDataVencimento()
        ];
    }

    public function json(): string
    {
        return json_encode(serialize());
    }

    public function getCnpjCpf(): string
    {
        return $this->cnpjCpf;
    }

    public function setCnpjCpf(string $cnpjCpf): void
    {
        $this->cnpjCpf = $cnpjCpf;
    }

    public function getCodigoTransacao(): string
    {
        return $this->codigoTransacao;
    }

    public function setCodigoTransacao(string $codigoTransacao): void
    {
        $this->codigoTransacao = $codigoTransacao;
    }

    public function getNumeroParcela(): int
    {
        return $this->numeroParcela;
    }

    public function setNumeroParcela(int $numeroParcela): void
    {
        $this->numeroParcela = $numeroParcela;
    }

    public function getNumeroDiasProrrogacao(): int
    {
        return $this->numeroDiasProrrogacao;
    }

    public function setNumeroDiasProrrogacao(int $numeroDiasProrrogacao): void
    {
        $this->numeroDiasProrrogacao = $numeroDiasProrrogacao;
    }
    
    public function getNovaDataVencimento(): string
    {
        return $this->novaDataVencimento;
    }

    public function setNovaDataVencimento(string $novaDataVencimento): void
    {
        $this->novaDataVencimento = $novaDataVencimento;
    }
}
