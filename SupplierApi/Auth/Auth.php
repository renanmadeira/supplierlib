<?php
declare(strict_types=1);

namespace SupplierApi\Auth;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use SupplierApi\Serializers\AuthSerializer as AuthSerializer;

class Auth
{
    protected $client;
    
    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }
    
    public function send(string $authorization, AuthSerializer $authSerializer): array
    {
        $datetime = date('Y-m-d H-i-s');
        try{
            $response = $this->client->post('oauth/access-token', [
                'Content-Type' => 'application/json',
                'Authorization' => 'Basic ' . $authorization
            ], $authSerializer->json());
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }
        
        $responseArray = json_decode($response->getBody());
        $responseArray['expires_in'] = date('Y-m-d H-i-s', strtotime(
            '+' . $responseArray['expires_in'] . ' seconds',
            $datetime
        ));
        return $responseArray;
    }
}
