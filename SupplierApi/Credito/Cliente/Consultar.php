<?php
declare(strict_types=1);

namespace SupplierApi\Credito\Cliente;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class Consultar
{
    protected $client;

    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }

    public function send(string $codGrupo, string $clientId, string $cnpjCpf): array
    {
        try{
            $response = $this->client->get('clientes/' . $codGrupo . '/' . $cnpjCpf . '/', [
                'Content-Type' => 'application/json',
                'client_id' => $clientId
            ]);
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }

        return json_decode($response->getBody());
    }
}
