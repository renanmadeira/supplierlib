<?php
declare(strict_types=1);

namespace SupplierApi\Credito\Cliente;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use SupplierApi\Serializers\DadosCadastrais as DadosCadastraisSerializer;


class AlteracaoDadosCadastrais
{
    protected $client;
    
    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }
    
    public function send(string $codGrupo, string $clientId, string $cnpjCpf, DadosCadastraisSerializer $dadosCadastrais): array
    {
        try{
            $response = $this->client->put('clientes/dadoscadastrais/' . $codGrupo . '/' . $cnpjCpf . '/', [
                'Content-Type' => 'application/json',
                'client_id' => $clientId
            ], $dadosCadastrais->json());
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }
        
        return json_decode($response->getBody());
    }
}
