<?php
declare(strict_types=1);

namespace SupplierApi\Credito\Analise;

use GuzzleHttp\Client as Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use SupplierApi\Serializers\ConcessaoLimite as ConcessaoSerializer;

class ConcessaoLimite
{
    protected $client;
    
    function __construct(string $baseUri)
    {
        $this->client = new Client(['base_uri' => $baseUri]);
    }
    
    public function send(string $codGrupo, string $clientId, ConcessaoSerializer $novoCliente): array
    {
        try{
            $response = $this->client->post('analises/concessao/' . $codGrupo . '/', [
                'Content-Type' => 'application/json',
                'client_id' => $clientId
            ], $novoCliente->json());
        } catch(Exception $e) {
            if ($e->hasResponse()) {
                throw new Exception(Psr7\str($e->getResponse()));
            }
            throw new Exception(Psr7\str($e->getRequest()));
        }
        
        return json_decode($response->getBody());
    }
}
